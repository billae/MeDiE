set linetype 1 lc rgb "#009900" pointtype 4 # "#555555"
set linetype 2 lc rgb "#FFCC33" pointtype 8 # "#CC1122"
set linetype 3 lc rgb "#9900CC" pointtype 2 # "#000099"
set linetype 4 lc rgb "#3377FF" pointtype 1 # "#FF8C00"
set linetype 5 lc rgb "#FF0099" pointtype 10 # "#BB00BB"
set linetype 6 lc rgb "#555555" pointtype 12 # "#009922"

set datafile separator ";"
set terminal postscript eps color

# ARG1 is the path and ARG2 is the type of run (sh, dh, indedh)

ymin= 0
ymax= 100
set yrange [ymin:ymax]


set xtics nomirror
set ytics nomirror

#set key box bottom right
set key box center right

#set title "Charge de travail pour chaque serveur"
set xlabel "nombre de serveur"
set ylabel "Pourcentage de charge moyen par serveur"

set output ARG1."/strong_scale.eps"

    plot ARG1.'/scale_curve.csv' using 2:xtic(1) w lp lw 2 title "pourcentage de charge moyen par serveur"
