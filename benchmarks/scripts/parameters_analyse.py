#!/bin/env python3

import sys
import pandas as pd
import matplotlib.pyplot as plt


#this script compute the exhaustive parameters analyse
#It takes in arguments the path of the  deviation_recap_all.csv file
if (len(sys.argv) < 2):
    print("please give the path of the deviation_recap file\n")
    sys.exit()

file_name = sys.argv[1]
df = pd.read_csv(file_name, delimiter=';')

#remove incomplete rows and runs without rebalancing
#df = df.dropna()
#only for dh (generating job mistake)
#df = df[df.redistribution_interval != 300]
#print (df.describe())
#only for windowed (generating job mistake)
#df = df[df.size_w != 60]
#

rebalancing_score = df['redistribution_useless'] / (df['redistribution_useful'] + df['redistribution_useless']) 
rebalancing_score = rebalancing_score.fillna(0) 

#normalization = (s -smin) / (smax - smin)
global_min_dev =  0
global_max_dev = df['global_max_deviation'].max()
deviation_av_score = ( df['deviation_av'] - global_min_dev ) / (global_max_dev - global_min_dev)


deviation_max_score = ( df['deviation_max'] - global_min_dev ) / (global_max_dev - global_min_dev) 

global_min_com = 0
global_max_com = df['global_max_cost'].max()
cost_score = ( df['cost'] - global_min_com ) / (global_max_com - global_min_com) 

com_score = 0.5 * cost_score + 0.5 * deviation_av_score

#add new columns in the dataframe
optional = pd.concat([rebalancing_score, deviation_av_score, deviation_max_score, com_score],axis=1 )
optional.columns = ['rebalancing_score', 'deviation_av_score', 'deviation_max_score', 'com_score']
#print (optional.describe())

newdf = pd.concat([df, optional], axis=1)



#By parameter analysis

#df_score_by_alpha = df_by_alpha['com_score'] 
#print(df_score_by_alpha.describe())

#newdf.boxplot(column = 'com_score', by = 'alpha')
#plt.grid(None)
#plt.xlabel("valeur de alpha")
#plt.ylabel("score")
#plt.title("communication score depending of alpha")


#fig = newdf.boxplot(column = 'com_score', by = 'size_w')
#plt.grid(None)
#plt.xlabel("valeur de la taille de la fenetre")
#labels = [10,30,60,120]
#fig.set_xticklabels(labels)
#plt.ylabel("score")
#plt.title("communication score depending of alpha")


#newdf.boxplot(column = 'com_score', by = 'percent')
#plt.grid(None)
#plt.xlabel("valeur de la marge")
#plt.ylabel("score")
#plt.title("communication score depending of alpha")

#newdf.boxplot(column = 'com_score', by = 'N_entry')
#plt.grid(None)
#plt.xlabel("valeur de N entry")
#plt.ylabel("score")
#plt.title("communication score depending of N_entry")

#newdf.boxplot(column = 'com_score', by = 'redistribution_interval')
#plt.grid(None)
#plt.xlabel("intervalle de redistribution")
#plt.ylabel("score")
#plt.title("communication score depending of the redistribution interval")

plt.show()

min_com = newdf['com_score'].min()
bests_com = newdf.loc[newdf['com_score'] == min_com,:]

#print(bests_com[['alpha','N_entry', 'redistribution_interval', 'cost','rebalancing_score', 'deviation_av_score', 'com_score']].describe())


#print the score for a particular value of a run
print(newdf)
