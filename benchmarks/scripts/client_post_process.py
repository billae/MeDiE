#!/bin/env python

import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#this script take time files (in arguments) and spreaded traces files
#and compute the mean time for processing a request during a period


#argument is the path of the run
if (len(sys.argv) < 4):
    print("please give the path of the run, the traces path and the number of step\n")
    sys.exit()

n_srv = 4
n_client = 64

output = sys.argv[1] + 'mean_time.csv'
output_curve = sys.argv[1] + 'tps_req.png'
time_files = sys.argv[1] + "client/vm"
trace_files = sys.argv[2]
n_step = int(sys.argv[3])

#each column is a vm
#each line is a time for a step
times = np.array([np.arange(float(n_client))]*n_step)

#each column is a vm
#each line is a number of request for a step
traces = np.array([np.arange(float(n_client))]*n_step)

#each column is a vm
#each line is a average throughput (number of request per second) for a step
means = np.array([np.arange(float(n_client))]*n_step)

#mean of throughtput for a step
total_mean = [0] * n_step

#string to write on file
s = ''


#fill the times array
for client in range(0, n_client):
    file_name = time_files + str (client + n_srv)
    f = open (file_name, "r")
    #discard the fisrt time value
    f.readline()
    times[:,client] = [line.strip() for line in f.readlines()]
    f.close()


for step in range(0, n_step):
    zero_req = 0
    for client in range(0, n_client):
        #fill the traces array
        file_name = trace_files + "-" + str(step) + "-" + str (client) + ".csv"
        try :
            traces[step, client] = sum(1 for line in open(file_name))
            means[step, client] = float(times[step, client]) / traces[step, client]
            #a = float(times[client][step]) / traces[client][step]
            total_mean[step] += means[step, client]

        except:
            means[step, client] = 0
            zero_req = zero_req + 1

        #print("step" + str(step) + " client" + str(client) + " = " + str(means[step, client]))

    if (zero_req != 0):
        total_mean[step] /= (n_client* - zero_req)
    else:
        total_mean[step] = 0

    s += str(total_mean[step]) + '\n'
    #for a in range(0,n_client):
    #    print("step" + str(step) + " client" + str(a) + " = " + str(means[a, step]))



#print(means)
df = pd.DataFrame(data=means)
df_trans = df.transpose()
#print(df)

label_btw_ticks=120
tick=5
labels = np.arange(tick, (n_step+1)*tick, label_btw_ticks)
#print(labels)
fig = df_trans.boxplot()
plt.ylim(0.0001,0.002)
plt.grid(None)
fig.set_xticklabels(labels)
fig.locator_params(axis='x', nbins=(n_step*tick/label_btw_ticks))
plt.savefig(output_curve)
#plt.show()
#output
with open(output, 'w') as fh:
    fh.write(s)
