set linetype 1 lc rgb "#009900" pointtype 4 # "#555555"
set linetype 2 lc rgb "#FFCC33" pointtype 8 # "#CC1122"
set linetype 3 lc rgb "#9900CC" pointtype 2 # "#000099"
set linetype 4 lc rgb "#3377FF" pointtype 1 # "#FF8C00"
set linetype 5 lc rgb "#FF0099" pointtype 10 # "#BB00BB"
set linetype 6 lc rgb "#555555" pointtype 12 # "#009922"

set datafile separator ";"
set terminal postscript eps color

# ARG1 is the path and ARG2 is the type of run (sh, dh, indedh)

ymin= 0e5
ymax= 5e6
set yrange [ymin:ymax]


set xtics nomirror
set ytics nomirror

#set key box top center
set key box center right

#set title "Charge de travail pour chaque serveur"
set xlabel "Temps (en minutes)"
set ylabel "Nombre de requetes recues"

set output ARG1."/average_load.eps"

if (ARG2 eq "sh") {
    plot ARG1.'/servers_load.csv' using (($0)*5):1 w lp lw 2 title "serveur 0",\
    ARG1.'/servers_load.csv' using (($0)*5):2 w lp lw 2 title "serveur 1",\
    ARG1.'/servers_load.csv' using (($0)*5):3 w lp lw 2 title "serveur 2",\
    ARG1.'/servers_load.csv' using (($0)*5):4 w lp lw 2 title "serveur 3"
}
if (ARG2 eq "indedh" || ARG2 eq "windowed" || ARG2 eq "dh") {
    plot ARG1.'/servers_load.csv' using (($0)*5):1 w lp lw 2 title "serveur 0",\
    ARG1.'/servers_load.csv' using (($0)*5):2 w lp lw 2 title "serveur 1",\
    ARG1.'/servers_load.csv' using (($0)*5):3 w lp lw 2 title "serveur 2",\
    ARG1.'/servers_load.csv' using (($0)*5):4 w lp lw 2 title "serveur 3",\
    ARG1.'/useful_rebalancing.txt' using (($1+1)*5):(ymax):1 w impulse title "redistribution utile" lt 3 lc rgb "green",\
    ARG1.'/useless_rebalancing.txt' using (($1+1)*5):(ymax):1 w impulse title "redistribution inutiles" lt 3 lc rgb "red"
}
